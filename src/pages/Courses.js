import {Fragment, useState, useEffect} from 'react';

// import coursesData from '../mockData/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	// checks is the import is successful
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);
	useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return(
					<CourseCard key={course.id} courseProp={course} />
				)
			}))
		})
	} ,[]);

	// const courses = coursesData.map(course => {
		// return(
			// <CourseCard key={course.id} courseProp = {course} />
		// )
	// })
	console.log(courses)

	// the "courseProp" in the CourseCard component is called "prop" which is a shorthand for "property"

	// the curly braces are used to signify that we are providing information using JS expressions rather than hard coded values which use double quotes

	return(	
		<Fragment>
			{courses}
		</Fragment>
	)
}