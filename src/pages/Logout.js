import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	// localStorage.clear();

	// consume the UserContext object and destructure it to access user state and unsetUser function
	const {unsetUser, setUser} = useContext(UserContext);

	// Clears local storage
	unsetUser();

	// placing the setUser setter function inside the useEffect is necessary because of updates within React JS that a state
	// of another component cannot be updated while trying to render a different component.
	// by adding the useEffect, this will allow the Logout page to render first before triggering the useEffect
	// which changes the state of our user
	useEffect(() => {
		setUser({id:null});
	})
	
	// Redirect back to login
	return(
		<Navigate to='/login' />
	)
}