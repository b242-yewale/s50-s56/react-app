import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(props){
	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [Password1, setPassword1] = useState('');
	const [Password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setisActive] = useState(false);

	// Check if values are successfully binded
	// console.log(email);
	// console.log(Password1);
	// console.log(Password2);

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({        
                email: email  	
            })
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
				    title:"Duplicate Email Found",
				    icon: "Failed",
				    text: "Please use another email"
				})
			}else{
        		fetch('http://localhost:4000/users/register', {
        			method: 'POST',
        			headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({    
                    	firstName: firstName,
                    	lastName: lastName,    
                        email: email,
                        mobileNo: mobileNo,
                        password: Password1   	
                    })
        		})
        		.then(res => res.json())
        		.then(data => {
        			console.log(data)
        			if (data === true){

        				setFirstName('');
        				setLastName('');
        				setEmail('');
        				setMobileNo('');
        				setPassword1('');
        				setPassword2('');

        				console.log("here")
        				Swal.fire({
        				    title:"Registration successfull",
        				    icon: "Success",
        				    text: "Welcome to Zuitt"
        				})

        				navigate("/login");       				
        			}else{
        				Swal.fire({
        				    title:"Something went wrong",
        				    icon: "Error",
        				    text: "Please try again"
        				})
        			}
        		})
            }
		})

		// localStorage.setItem('email', email);

		// alert('Thank you for your registration!');
	}

	


	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !=='' && lastName !== '' && mobileNo !== '' && email !== '' && Password1 !== '' && Password2 !== '') && (Password1 === Password2)){
			setisActive(true);
		}
		else{
			setisActive(false);
		}
	}, [email, Password1, Password2]);

	return(
		(user.id !== null)?
		<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group className="mb-3" controlId="first name">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    	type="first name" 
			    	placeholder="First Name"
			    	value={firstName}
			    	onChange={e => setFirstName(e.target.value)}
			    	required 
			    />
			</Form.Group>

			<Form.Group className="mb-3" controlId="last name">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			    	type="last name" 
			    	placeholder="Last Name"
			    	value={lastName}
			    	onChange={e => setLastName(e.target.value)}
			    	required 
			    />
			</Form.Group>

			<Form.Group className="mb-3" controlId="mobile no">
			    <Form.Label>Mobile Number (10 digits)</Form.Label>
			    <Form.Control 
			    	type="mobile no" 
			    	placeholder="Mobile No"
			    	value={mobileNo}
			    	onChange={e => setMobileNo(e.target.value)}
			    	required 
			    />
			</Form.Group>

		    <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={e => setEmail(e.target.value)}
		        	required
		        />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		    </Form.Group>

		    <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	value={Password1}
		        	onChange={e => setPassword1(e.target.value)}
		        	required 
		        />
		    </Form.Group>

		    <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Retype Password"
		        	value={Password2}
		        	onChange={e => setPassword2(e.target.value)}
		        	required 
		        />
		    </Form.Group>

		    {/*conditional render submit button based on isActive state*/}
		    { isActive ? 
		    	<Button variant="primary" type="submit" id="SubmitBtn">
		    	    Submit
		    	</Button>
		    	:
		    	<Button variant="danger" type="submit" id="SubmitBtn" disabled>
		    	    Submit
		    	</Button>
		    }
		    
		</Form>
	)
}