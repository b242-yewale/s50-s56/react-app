import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(props) {
    // Allows us to consume the User context object and its properties to use for user validation
    const {user, setUser} = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch requeest to the corresponding backend API
        // the header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON

        // Fetch request that we have will communicate with our backend application providing it with a stringify JSON
        // converting the information retrieved from the backend into a JS obkect using ".then(res => res.json())"
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            // good pratie to check for the value being sent as response if it is correct information needed in the frontend application.

            // if no information is found, the 'access' property will not be availableand will retund undefined
            // using tyoeof operator will return a string of the data type of the variable/expression which is why the value being compared is in the string data type
            if (typeof data.acces !== "undefined"){
                // the JWT will be used to retrieve user information access the whole frontend application, storing it in localstorage
                localStorage.setItem('token', data.acces)
                retrieveUserDetails(data.acces);

                Swal.fire({
                    title:"Login Successful",
                    icon: "Success",
                    text: "Welcome to Zuitt!"
                })
            }else{
                Swal.fire({
                    title:"Authentication Failed",
                    icon: "Error",
                    text: "Check your login credentials and try again."
                })
            }
        })

        // Set the email of the authenticated user in the local storage
        /* Syntax:
            localStorage.setItem('propertyName', value);
        */
        // localStorage.setItem('email', email);

        // though access to use information can be done via localstorage, this is necessary to update the user state
        // which will help update the App component and rerender it to avoid refreshing the page upon user Login/Logout

        setUser({
            email:localStorage.getItem('email')
        })

        // Clear input fields after submission
        setEmail('');
        setPassword('');

        // alert(`${email} has been verified! Welcome back!`);

    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data.id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);


    return (
        (user.id !== null)?
        <Navigate to="/courses" />
        :
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

             { isActive ? 
                 <Button variant="primary" type="submit" id="submitBtn">
                     Submit
                 </Button>
                 : 
                 <Button variant="danger" type="submit" id="submitBtn" disabled>
                     Submit
                 </Button>
             }

        </Form>
    )
}

