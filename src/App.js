import AppNavbar from './components/AppNavbar';
// import {Fragment} from 'react';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Error from './pages/Error';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  // Check if the user information is properly stored uopn Login and the localStorage information is cleared uopn Logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>  
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            {/*<Route path="/courseView" element={<CourseView />} />*/}
            <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
