import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';
// import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}) {


	// we will be using state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	// SYNTAX:
		// const [getter, setter] = useState(initialGetterValue)
	// const [ count, setCount ] = useState(0);   (commented out after using our courses database)
	// console.log(useState(0));

	// checks the value of props
	console.log(courseProp)
	// console.log(typeof courseProp)
	
	// Deconstruct the courseProp properties into their own variables
	const { _id, name, description, price } = courseProp;
	// console.log(name);
	// console.log(description);
	// console.log(price);

	// Function that keeps track of the enrollees for a course
	// By default JavaScript is synchronous it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
	// The setter function for useStates are asynchronous allowing it to execute separately from other codes in the program
	// The "setCount" function is being executed while the "console.log" is already completed resulting in the value to be displayed in the console to be behind by one count

	// Use state hook for getting and setting the seats for this course
	// const [seats, setSeats] = useState(10);
	// function enroll(){
	// 	if(seats > 0){
	// 		setCount(count + 1);
	// 		console.log(`Enrollees ${count}`)
	// 		setSeats(seats - 1);
	// 		console.log('Seats: ' + seats);
	// 	}
	// 	else{
	// 		alert("No more seats available");
	// 	}	
	// }

	// const [isOpen, setIsOpen] = useState(true);

	// useEffect(() => {
	// 	if(seats === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
                {/*<Card.Text>Enrollees: {count}</Card.Text>*/}
                {/*<Card.Text>Seats: {seats}</Card.Text>*/}
                {/*<Button className="bg-primary" onClick={enroll}>Enroll</Button>*/}
            </Card.Body>
        </Card>
    )
}

CourseCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific "shape"
    course: PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
